package myApp.spring.mvc.dao;

import myApp.spring.mvc.model.Customer;

public interface CustomerDAO {
	void saveCustomer(Customer theCustomer);
}
