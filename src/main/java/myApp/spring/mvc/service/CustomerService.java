package myApp.spring.mvc.service;

import myApp.spring.mvc.model.Customer;

public interface CustomerService {
	public void saveCustomer(Customer theCustomer);
}
